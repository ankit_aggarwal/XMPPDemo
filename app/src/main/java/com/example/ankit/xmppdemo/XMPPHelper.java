package com.example.ankit.xmppdemo;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.ChatManagerListener;
import org.jivesoftware.smack.chat.ChatMessageListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.filetransfer.FileTransferManager;
import org.jivesoftware.smackx.filetransfer.FileTransferNegotiator;
import org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer;

import java.io.File;
import java.io.IOException;

public class XMPPHelper {

    private static Context mContext;
    private static Handler mHandler;
    private XMPPTCPConnection mConnection;
    private FileTransferManager fileTransferManager;
    private ChatManager mChatManager;
    private Chat mChat;

    private final static String DOMAIN = "172.16.1.236";
    private final static int PORT = 5222;       //eJaber listens at port 5222 by default

    public XMPPHelper(Context context, String userName, String password) {
        mContext = context;
        mHandler = new MessageHandler(Looper.getMainLooper());
        init(userName, password);
    }

    private void init(String userName, String password) {
        XMPPTCPConnectionConfiguration.Builder builder = XMPPTCPConnectionConfiguration.builder()
                .setCompressionEnabled(true)
                .setConnectTimeout(10000)
                .setSecurityMode(XMPPTCPConnectionConfiguration.SecurityMode.disabled)
                .setUsernameAndPassword(userName, password)
                .setServiceName(DOMAIN)
                .setHost(DOMAIN)
                .setPort(PORT)
                .setDebuggerEnabled(true);
        //.setSocketFactory(SSLSocketFactory.getDefault());
        mConnection = new XMPPTCPConnection(builder.build());
        //mConnection.addConnectionListener(this);

        //set chat manager
        setChatManager();

        //init file transfer manager
        initFileTransferManager();
    }

    private void setChatManager() {
        mChatManager = ChatManager.getInstanceFor(mConnection);
        mChatManager.addChatListener(new ChatManagerListener() {
            @Override
            public void chatCreated(Chat chat, boolean createdLocally) {
                chat.addMessageListener(new ChatMessageListener() {
                    @Override
                    public void processMessage(Chat chat, Message message) {
                        android.os.Message msg = new android.os.Message();
                        Bundle data = new Bundle();
                        if (TextUtils.isEmpty(message.getBody())) {
                            data.putString("message", "");
                        } else {
                            data.putString("message", message.getFrom() + " : " + message.getBody());
                        }
                        msg.setData(data);
                        mHandler.sendMessage(msg);
                    }
                });
            }
        });
    }

    private void createChat(String participant) {
        mChat = mChatManager.createChat(participant);
    }

    private void initFileTransferManager() {
        fileTransferManager = FileTransferManager.getInstanceFor(mConnection);
        FileTransferNegotiator.getInstanceFor(mConnection);
    }

    public void sendMessage(String message, String to) throws XMPPException {
        String participant = to + "@" + DOMAIN;

        if (mChat == null) {
            createChat(participant);
        }

        try {
            mChat.sendMessage(message);
        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }
    }

    /*public void sendFile(String to) {
        String destination = to + "@" + DOMAIN + "/spark 2.6.3";
        // Create the outgoing file transfer
        OutgoingFileTransfer transfer = fileTransferManager.createOutgoingFileTransfer(destination);

        // Send the file
        try {
            File file = new File(Environment.getExternalStorageDirectory().getPath() + "/Pictures/xyz.jpeg");
            transfer.sendFile(file, "You won't believe this!");
        } catch (SmackException e) {
            e.printStackTrace();
        }
        System.out.println("Status :: " + transfer.getStatus() + " Error :: "
                + transfer.getError() + " Exception :: "
                + transfer.getException());
        System.out.println("Is it done? " + transfer.isDone());
    }*/

    public void login() throws XMPPException {
        //connect and login in background thread to protect "NetworkOnMainThread" Exception
        new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... params) {
                try {
                    mConnection.connect();
                    mConnection.login();
                } catch (SmackException | IOException | XMPPException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    public void disconnect() {
        if (mConnection.isConnected()) {
            mConnection.disconnect();
        }
    }

    public boolean isConnected() {
        if (mConnection != null) {
            return mConnection.isConnected();
        }
        return false;
    }

    private static class MessageHandler extends Handler {

        MessageHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(android.os.Message msg) {
            super.handleMessage(msg);
            ((IncomingMessageListener) mContext).printMessage(msg.getData().getString("message"));
        }
    }

    /*
    public void displayBuddyList() {
        Roster roster = mConnection.getRoster();
        Collection<RosterEntry> entries = roster.getEntries();

        System.out.println("\n\n" + entries.size() + " buddy(ies):");
        for (RosterEntry r : entries) {
            System.out.println(r.getUser());
        }
    }

    public void processMessage(Chat chat, Message message) {
        if (message.getType() == Message.Type.chat)
            System.out.println(chat.getParticipant() + " says: " + message.getBody());
    }

    @Override
    public void connected(XMPPConnection connection) {
    }

    @Override
    public void authenticated(XMPPConnection connection, boolean resumed) {
    }

    @Override
    public void connectionClosed() {
    }

    @Override
    public void connectionClosedOnError(Exception e) {
    }

    @Override
    public void reconnectionSuccessful() {
    }

    @Override
    public void reconnectingIn(int seconds) {
    }

    @Override
    public void reconnectionFailed(Exception e) {
    }
    */
}