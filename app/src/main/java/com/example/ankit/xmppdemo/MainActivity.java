package com.example.ankit.xmppdemo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import org.jivesoftware.smack.XMPPException;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements IncomingMessageListener {

    private TextView tvTyping;

    private ArrayList<String> mMessageList;
    private ArrayAdapter mAdapter;
    private XMPPHelper mXMPPHelper;

    private final static String USER1 = "test1";
    private final static String USER2 = "test2";
    private final static String PASSWORD = "test123";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tvTyping = (TextView) findViewById(R.id.tv_typing);
        final ListView lvMessages = (ListView) findViewById(R.id.lv_messages);
        final EditText etMessage = (EditText) findViewById(R.id.et_message);
        Button btnSend = (Button) findViewById(R.id.btn_send);

        //set mAdapter
        mMessageList = new ArrayList<>();
        mAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, mMessageList);
        lvMessages.setAdapter(mAdapter);

        mXMPPHelper = new XMPPHelper(this, USER2, PASSWORD);

        //set listeners
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = etMessage.getText().toString().trim();
                if (mXMPPHelper.isConnected() && !TextUtils.isEmpty(message)) {
                    try {
                        mXMPPHelper.sendMessage(message, USER1);
                        //mXMPPHelper.sendFile(USER1);
                        mMessageList.add(USER2 + " : " + message);
                        mAdapter.notifyDataSetChanged();
                    } catch (XMPPException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            mXMPPHelper.login();
        } catch (XMPPException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        mXMPPHelper.disconnect();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mXMPPHelper.disconnect();
    }

    @Override
    public void printMessage(String message) {
        if (TextUtils.isEmpty(message)) {
            tvTyping.setVisibility(View.VISIBLE);
        } else {
            tvTyping.setVisibility(View.GONE);
            mMessageList.add(message);
            mAdapter.notifyDataSetChanged();
        }
    }


}
