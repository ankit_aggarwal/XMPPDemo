package com.example.ankit.xmppdemo;

interface IncomingMessageListener {
    void printMessage(String message);
}
